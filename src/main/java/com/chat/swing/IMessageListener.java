package com.chat.swing;

import com.chat.Message;

public interface IMessageListener {
    void messageArrived(Message message);
}
