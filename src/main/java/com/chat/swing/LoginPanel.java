package com.chat.swing;

import javax.swing.*;
import java.awt.*;

import static java.awt.GridBagConstraints.HORIZONTAL;


public class LoginPanel extends JPanel {
    private ILoginListener loginListener;
    private JTextField fieldUsername;
    private JTextField fieldPort;
    private JButton gZatwierdz;

    public LoginPanel(ILoginListener okienko) {
        this.loginListener = okienko;
        setLayout(new GridBagLayout());

        dodajEtykieteIPoleLogin();
        dodajEtykieteIPolePort();
        dodajGuzikZatwierdz();

        setPreferredSize(new Dimension(320, 240));
    }

    private void dodajGuzikZatwierdz() {
        gZatwierdz = new JButton("Zatwierdź");
        gZatwierdz.addActionListener((actionEvent) -> {
            String login = fieldUsername.getText();
            int port = Integer.parseInt(fieldPort.getText());

            loginListener.login(login, port);
        });

        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 0;
        c1.gridy = 2;
        c1.fill = GridBagConstraints.HORIZONTAL;
        c1.weightx = 2.0;
        c1.weighty = 0.0;
        c1.gridwidth = 2;
        add(gZatwierdz, c1);
    }

    private void dodajEtykieteIPolePort() {
        JLabel etykieta = new JLabel("Port:");
        fieldPort = new JTextField();

        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 1;
        c1.gridy = 0;
        c1.fill = GridBagConstraints.HORIZONTAL;
        c1.weightx = 1.0;
        c1.weighty = 0.0;
        add(etykieta, c1);

        GridBagConstraints c2 = new GridBagConstraints();
        c2.gridx = 1;
        c2.gridy = 1;
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.weightx = 1.0;
        c2.weighty = 0.0;
        add(fieldPort, c2);
    }

    private void dodajEtykieteIPoleLogin() {
        JLabel etykieta = new JLabel("Login:");
        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 0;
        c1.gridy = 0;
        c1.fill = GridBagConstraints.HORIZONTAL;
        c1.weightx = 1.0;
        c1.weighty = 0.0;
        add(etykieta, c1);

        fieldUsername = new JTextField();
        GridBagConstraints c2 = new GridBagConstraints();
        c2.gridx = 0;
        c2.gridy = 1;
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.weightx = 1.0;
        c2.weighty = 0.0;
        add(fieldUsername, c2);
    }
}
