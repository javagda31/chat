package com.chat.swing;

import javax.swing.*;
import java.awt.*;

import static java.awt.GridBagConstraints.BOTH;

public class Okienko extends JFrame implements ILoginListener {
    private JPanel mainPanel;
    private LoginPanel loginPanel;

    public Okienko() throws HeadlessException {
        this.mainPanel = new JPanel(new GridBagLayout());

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;

        this.loginPanel = new LoginPanel(this);
        this.mainPanel.add(this.loginPanel, constraints);

        setContentPane(this.mainPanel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(640, 480);
    }

    @Override
    public void login(String username, int port) {
        // usuwamy panel logowania.
        this.mainPanel.remove(this.loginPanel);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.fill = BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;

        this.mainPanel.add(new ChatPanel(username, port), constraints);
        this.revalidate();
        this.repaint();
    }
}
