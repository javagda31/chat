package com.chat.swing;

import com.chat.Message;
import com.chat.udp.UDPSocketListener;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ChatPanel extends JPanel implements IMessageListener {
    private JList<String> list;
    private DefaultListModel<String> listModel;
    private JScrollPane scrollPane;
    private JTextField poleWiadomosci;
    private JButton guzik;

    private final String username;
    private final int port;

    public ChatPanel(String s, int port) {
        this.username = s;
        this.port = port;
        setLayout(new GridBagLayout());

        dodajEtykiete(s);
        dodajChatView();
        dodajPanelWiadomosc();

        UDPSocketListener listener =
                new UDPSocketListener(port, this);
        listener.start();
    }

    private void dodajPanelWiadomosc() {
        JLabel etykieta = new JLabel("Wiadomosc:");
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(etykieta, constraints);

        poleWiadomosci = new JTextField();
        poleWiadomosci.addActionListener(new SendActionListener());
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.weightx = 0.9;
        constraints.weighty = 0.00;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.BOTH;
        add(poleWiadomosci, constraints);

        guzik = new JButton(">");
        guzik.addActionListener(new SendActionListener());
        constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.weightx = 0.1;
        constraints.weighty = 0.00;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.BOTH;
        add(guzik, constraints);
    }

    private void dodajChatView() {
        listModel = new DefaultListModel<>();
        list = new JList<>(listModel);

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.fill = GridBagConstraints.BOTH;

        add(scrollPane, constraints);
    }

    private void dodajEtykiete(String username) {
        JLabel etykieta = new JLabel("Jesteś zalogowany jako:" + username);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.fill = GridBagConstraints.HORIZONTAL;

        add(etykieta, constraints);
    }

    private void wyslij(Message msg, int port) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        socket.setBroadcast(true);

        ObjectMapper mapper = new ObjectMapper();
        String jsonMessage = mapper.writeValueAsString(msg);

        // tworzymy pakiet z wiadomością.
        DatagramPacket pakiet = new DatagramPacket(jsonMessage.getBytes(),
                jsonMessage.length(),
                InetAddress.getByName("255.255.255.255"), // oznacza adres całej naszej sieci
                port); // port zdefiniowany w parametrze

        // wysyłamy pakiet
        socket.send(pakiet);
        socket.close();
    }

    @Override
    public void messageArrived(Message message) {
        String msg = message.getUser() + ">" + message.getMessage();
        listModel.addElement(msg);

        int lastIndex = listModel.getSize()-1;
        list.ensureIndexIsVisible(lastIndex);
    }

    private class SendActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String wiadomosc = poleWiadomosci.getText();
            poleWiadomosci.setText("");

            Message wiadomoscMessage = new Message(username, wiadomosc);
            try {
                wyslij(wiadomoscMessage, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
