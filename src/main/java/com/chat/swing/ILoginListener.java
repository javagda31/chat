package com.chat.swing;

public interface ILoginListener {
    void login(String username, int port);
}
