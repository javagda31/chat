package com.chat.udp;

import com.chat.Message;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        UDPSocketListener listener = new UDPSocketListener(6666, this);
//        listener.start();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Cześć, jak się nazywasz?");
        String nazwaUzytkownika = scanner.nextLine();

        String wiadomosc;

        System.out.println("Witaj w czacie. Kolejne wiadomości zatwierdzaj znakiem [Enter]:");
        do{
            wiadomosc = scanner.nextLine();

            Message msg = new Message(nazwaUzytkownika, wiadomosc);
            try {
                wyslij(msg, 6666);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }while (!wiadomosc.equalsIgnoreCase("koniec"));
    }

    private static void wyslij(Message msg, int port) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        socket.setBroadcast(true);

        ObjectMapper mapper = new ObjectMapper();
        String jsonMessage = mapper.writeValueAsString(msg);

        // tworzymy pakiet z wiadomością.
        DatagramPacket pakiet = new DatagramPacket(jsonMessage.getBytes(),
                jsonMessage.length(),
                InetAddress.getByName("255.255.255.255"), // oznacza adres całej naszej sieci
                port); // port zdefiniowany w parametrze

        // wysyłamy pakiet
        socket.send(pakiet);
        socket.close();
    }

}
