package com.chat.udp;

import com.chat.Message;
import com.chat.swing.ChatPanel;
import com.chat.swing.IMessageListener;
import com.fasterxml.jackson.core.io.JsonEOFException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UDPSocketListener implements Runnable {
    private ExecutorService thread = Executors.newCachedThreadPool();
    private DatagramSocket socket;
    private int port;
    private boolean working;
    private IMessageListener msgListener;

    public UDPSocketListener(int port, IMessageListener msgListener) {
        this.msgListener = msgListener;
        this.port = port;
    }

    public void start() {
        working = true;
        thread.submit(this);
    }

    @Override
    public void run() {
        try {
            // Datagram socket to obiekt który zdolny jest do odbierania danych na zadanym porcie.
            // definiujemy go z portem zdefiniowanym w konstruktorze.
            socket = new DatagramSocket(port);

            while (working) {
                // tworzymy pakiet którym będziemy odbierać dane.
                DatagramPacket packet = new DatagramPacket(new byte[65000], 65000);

                // odebranie pakietu
                socket.receive(packet);

                // przepisanie bajtów do string'a
                String wiadomosc = new String(packet.getData(), 0, packet.getLength());

                try {
                    Message msg = new ObjectMapper().readValue(wiadomosc, Message.class);
                    msgListener.messageArrived(msg);
                }catch (JsonEOFException| JsonMappingException jeof){
                    System.err.println("Wiadomość zignorowana: " + wiadomosc);
                }
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
