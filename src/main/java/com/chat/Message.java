package com.chat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.format.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Message {
    private String time;

    private String user;
    private String message;

    public Message(String user, String message) {
        this.time = String.valueOf(System.currentTimeMillis());
        this.user = user;
        this.message = message;
    }
}
